/**
 * Application functionality
 * @file app.js
 */

'use strict'

const key = 'token'

let app = {
  token: null,
  container: null,
  init: function () {
    this.container = $('#message-container')
    this.initToken()
    if (this.token) this.verify()
  },
  initToken: function () {
    this.token = this.getParameterByName(key)
  },
  getParameterByName: function (name) {
    let match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search)
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '))
  },
  verify: function () {
    $.post(
      config.url + '/verify',
      { token: this.token },
       this.success.bind(this)
     ).fail(this.error.bind(this))
  },
  success: function (data) {
    this.container.html('<strong>Correo validado.<strong>')
  },
  error: function (err) {
    if (err.status === 400) {
      this.container.html('<strong>Token inválido. No se puede validar con ese correo.<strong>')
    }
  }
}
app.init()
