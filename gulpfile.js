/**
 * Verification Interface
 * @description Gulp tasks
 * @file gulpfile.js
 * @author Joel Cano
 */

'use strict'
// Dependencies
const del = require('del')
const gulp = require('gulp')
const util = require('gulp-util')
const sass = require('gulp-sass')
const babel = require('gulp-babel')
const concat = require('gulp-concat')
const rename = require('gulp-rename')
const uglify = require('gulp-uglify')
const cssnano = require('gulp-cssnano')
// const imagemin = require('gulp-imagemin')
const standard = require('gulp-standard')
const browserSync = require('browser-sync')
const runSequence = require('run-sequence')
const sourcemaps = require('gulp-sourcemaps')
const handlebars = require('gulp-compile-handlebars')
// Constants
const paths = {
  build: 'build',
  config: 'src/config',
  js: 'src/js/**/*.js',
  images: 'src/img/**/*',
  partials: 'src/partials',
  sass: 'src/sass/**/*.scss',
  views: 'src/views/**/*.hbs',
  jquery: 'node_modules/jquery/dist/jquery.min.js',
  skeleton: [
    'node_modules/skeleton-css/css/normalize.css',
    'node_modules/skeleton-css/css/skeleton.css'
  ]
}
const all = ['styles', 'images', 'js', 'html']
const branches = {
  master: 'stage',
  production: 'prod'
}
/**
 * Build folder erase task.
 */
gulp.task('clean', function () {
  return del([paths.build])
})
/**
 * Image optimization task.
 */
gulp.task('images', function () {
  return gulp.src(paths.images)
    // .pipe(imagemin({ optimizationLevel: 5 }))
    .pipe(gulp.dest(`${paths.build}/img`))
})
/**
 * Skeleton compile task.
 */
gulp.task('skeleton', function () {
  return gulp.src(paths.skeleton)
    .pipe(sourcemaps.init())
      // .pipe(cssnano())
      .pipe(concat('vendor.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(`${paths.build}/css`))
})
/**
 * Skeleton compile task.
 */
gulp.task('jquery', function () {
  return gulp.src(paths.jquery)
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest(`${paths.build}/js`))
})
/**
 * Sass compile task.
 */
gulp.task('styles', ['skeleton'], function () {
  return gulp.src(paths.sass)
    .pipe(sourcemaps.init())
      .pipe(sass({
        outputStyle: 'compressed'
      }).on('error', sass.logError))
      .pipe(cssnano())
      .pipe(concat('app.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(`${paths.build}/css`))
})
/**
 * Standard linter task.
 */
gulp.task('standard', function () {
  return gulp.src(paths.js)
    .pipe(standard())
    .pipe(standard.reporter('default', {
      quiet: true,
      breakOnError: true,
      breakOnWarning: true
    }))
})
/**
 * JS transpile and compile task.
 */
gulp.task('js', ['jquery', 'standard'], function () {
  let env = null
  if (branches[util.env.env]) env = branches[util.env.env]
  let path = paths.config + '/config'
  if (env) path = path + '.' + env
  path = path + '.js'
  return gulp.src([path, paths.js])
    .pipe(sourcemaps.init())
      .pipe(babel({ presets: ['es2015'] }))
      .pipe(uglify())
      .pipe(concat('app.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(`${paths.build}/js`))
})
/**
 * Handlebars compile task.
 */
gulp.task('html', function () {
  return gulp.src(paths.views)
    .pipe(handlebars({}, {
      ignorePartials: true,
      batch: [paths.partials]
    }))
    .pipe(rename({ extname: '.html' }))
    .pipe(gulp.dest(paths.build))
})
/**
 * Watch task.
 */
gulp.task('watch', ['clean'], function () {
  browserSync.init({
    server: paths.build
  })
  gulp.watch(paths.js, ['js']).on('change', browserSync.reload)
  gulp.watch(paths.images, ['images']).on('change', browserSync.reload)
  gulp.watch(paths.sass, ['styles']).on('change', browserSync.reload)
  gulp.watch(paths.views, ['html']).on('change', browserSync.reload)
  runSequence(all)
})
/**
 * Default task. Called when `gulp` is run from cli.
 */
gulp.task('default', all)
