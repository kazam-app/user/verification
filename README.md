# Verification Interface
[![build status](https://git.urbanity.xyz/kazam/user/verification/badges/master/build.svg)](https://git.urbanity.xyz/kazam/user/verification/commits/master)

## Dependencies
- yarn
- gulp

## Install

```sh
yarn install
```

## Run

```sh
gulp
```

## Build

```sh
gulp build
```

## Deploy

AWS S3
